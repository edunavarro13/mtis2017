
/**
 * Onlyeat_utilsSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
    package org.example.www.onlyeat_utils;
    import java.sql.*;
    import java.util.ArrayList;
    import java.util.Date;
    import java.util.List;
    /**
     *  Onlyeat_utilsSkeleton java skeleton for the axisService
     */
    public class Onlyeat_utilsSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param obtenerBdLocal 
             * @return obtenerBdLocalResponse 
         */
        
    	  public org.example.www.onlyeat_utils.ObtenerBdLocalResponse obtenerBdLocal
          (
          org.example.www.onlyeat_utils.ObtenerBdLocal obtenerBdLocal
          )
    {
        	Connection connect = null;
            Statement statement = null;
            ObtenerBdLocalResponse respuesta = new ObtenerBdLocalResponse();
         	try {  	          	
             	Class.forName("com.mysql.jdbc.Driver");
             	connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
             	statement = connect.createStatement();   	
                     statement.execute("use proyectomtis2017");
                     ResultSet resultSet = statement.executeQuery("SELECT bd FROM `locales` WHERE id = '"+ obtenerBdLocal.getId() +"'");    
                     while (resultSet.next())
                     {               
                       respuesta.setBd(resultSet.getString("bd"));
                     }
                     statement.close();
                     connect.close();
                     
             } catch (Exception e){  System.out.println (e);}
         	  
         		return respuesta;        
       }
         
        /**
         * Auto generated method signature
         * 
                                     * @param crearJson 
             * @return crearJsonResponse 
         */
        
                 public org.example.www.onlyeat_utils.CrearJsonResponse crearJson
                  (
                  org.example.www.onlyeat_utils.CrearJson crearJson
                  )
            {
               CrearJsonResponse respuesta = new CrearJsonResponse();
               respuesta.setOut("{\"fecha\":\""+crearJson.getFecha()+"\",\"cantidad\":\""+crearJson.getCantidad()+"\"}");
               return respuesta;
        }
     
    }
    