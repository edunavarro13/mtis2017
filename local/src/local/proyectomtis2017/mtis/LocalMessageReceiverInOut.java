
/**
 * LocalMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package local.proyectomtis2017.mtis;

        /**
        *  LocalMessageReceiverInOut message receiver
        */

        public class LocalMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        LocalSkeleton skel = (LocalSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("pagarLocal".equals(methodName)){
                
                local.proyectomtis2017.mtis.PagarLocalResponse pagarLocalResponse13 = null;
	                        local.proyectomtis2017.mtis.PagarLocal wrappedParam =
                                                             (local.proyectomtis2017.mtis.PagarLocal)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    local.proyectomtis2017.mtis.PagarLocal.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               pagarLocalResponse13 =
                                                   
                                                   
                                                         skel.pagarLocal(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), pagarLocalResponse13, false, new javax.xml.namespace.QName("mtis.proyectomtis2017/local/",
                                                    "pagarLocal"));
                                    } else 

            if("ultimaFechaPago".equals(methodName)){
                
                local.proyectomtis2017.mtis.UltimaFechaPagoResponse ultimaFechaPagoResponse15 = null;
	                        local.proyectomtis2017.mtis.UltimaFechaPago wrappedParam =
                                                             (local.proyectomtis2017.mtis.UltimaFechaPago)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    local.proyectomtis2017.mtis.UltimaFechaPago.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               ultimaFechaPagoResponse15 =
                                                   
                                                   
                                                         skel.ultimaFechaPago(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), ultimaFechaPagoResponse15, false, new javax.xml.namespace.QName("mtis.proyectomtis2017/local/",
                                                    "ultimaFechaPago"));
                                    } else 

            if("totalPedidosRango".equals(methodName)){
                
                local.proyectomtis2017.mtis.TotalPedidosRangoResponse totalPedidosRangoResponse17 = null;
	                        local.proyectomtis2017.mtis.TotalPedidosRango wrappedParam =
                                                             (local.proyectomtis2017.mtis.TotalPedidosRango)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    local.proyectomtis2017.mtis.TotalPedidosRango.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               totalPedidosRangoResponse17 =
                                                   
                                                   
                                                         skel.totalPedidosRango(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), totalPedidosRangoResponse17, false, new javax.xml.namespace.QName("mtis.proyectomtis2017/local/",
                                                    "totalPedidosRango"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.PagarLocal param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.PagarLocal.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.PagarLocalResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.PagarLocalResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.UltimaFechaPago param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.UltimaFechaPago.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.UltimaFechaPagoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.UltimaFechaPagoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.TotalPedidosRango param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.TotalPedidosRango.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(local.proyectomtis2017.mtis.TotalPedidosRangoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(local.proyectomtis2017.mtis.TotalPedidosRangoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, local.proyectomtis2017.mtis.PagarLocalResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(local.proyectomtis2017.mtis.PagarLocalResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private local.proyectomtis2017.mtis.PagarLocalResponse wrappagarLocal(){
                                local.proyectomtis2017.mtis.PagarLocalResponse wrappedElement = new local.proyectomtis2017.mtis.PagarLocalResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, local.proyectomtis2017.mtis.UltimaFechaPagoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(local.proyectomtis2017.mtis.UltimaFechaPagoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private local.proyectomtis2017.mtis.UltimaFechaPagoResponse wrapultimaFechaPago(){
                                local.proyectomtis2017.mtis.UltimaFechaPagoResponse wrappedElement = new local.proyectomtis2017.mtis.UltimaFechaPagoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, local.proyectomtis2017.mtis.TotalPedidosRangoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(local.proyectomtis2017.mtis.TotalPedidosRangoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private local.proyectomtis2017.mtis.TotalPedidosRangoResponse wraptotalPedidosRango(){
                                local.proyectomtis2017.mtis.TotalPedidosRangoResponse wrappedElement = new local.proyectomtis2017.mtis.TotalPedidosRangoResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (local.proyectomtis2017.mtis.PagarLocal.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.PagarLocal.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (local.proyectomtis2017.mtis.PagarLocalResponse.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.PagarLocalResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (local.proyectomtis2017.mtis.TotalPedidosRango.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.TotalPedidosRango.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (local.proyectomtis2017.mtis.TotalPedidosRangoResponse.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.TotalPedidosRangoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (local.proyectomtis2017.mtis.UltimaFechaPago.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.UltimaFechaPago.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (local.proyectomtis2017.mtis.UltimaFechaPagoResponse.class.equals(type)){
                
                        return local.proyectomtis2017.mtis.UltimaFechaPagoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    