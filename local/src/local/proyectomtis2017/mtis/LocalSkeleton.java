
/**
 * LocalSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
    package local.proyectomtis2017.mtis;
    import java.sql.*;
    import java.util.ArrayList;
    import java.util.Date;
    import java.util.List;

/**
     *  LocalSkeleton java skeleton for the axisService
     */
    public class LocalSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param pagarLocal 
             * @return pagarLocalResponse 
         */
        
    	 public local.proyectomtis2017.mtis.PagarLocalResponse pagarLocal
         (
         local.proyectomtis2017.mtis.PagarLocal pagarLocal
         )
   {
       	 Connection connect = null;
         	 Statement statement = null;
         	 PagarLocalResponse respuesta = new PagarLocalResponse();
         	 String ped;
         	 float pago = pagarLocal.getPrecioTotalPedidos() * 0.8F;
         	 try {  	
         		 
         		Class.forName("com.mysql.jdbc.Driver");
            	connect = DriverManager.getConnection(pagarLocal.getBd(), "root", "");
           	statement = connect.createStatement();   	
           	statement.execute("INSERT INTO pagos (pago) VALUES ('"+pago+"')");       
               statement.close();
             	statement = connect.createStatement();   	
             	ResultSet resultSet = statement.executeQuery("SELECT FECHA, PAGO FROM PAGOS WHERE FECHA = (select MAX(FECHA) from pagos)");
               if (resultSet.next())
               {
                 	 respuesta.localFecha = resultSet.getString("fecha");
                 	 respuesta.localCantidad = resultSet.getFloat("pago");
               }
             	 statement.close();
             	 connect.close();
         	        
         	 } catch (Exception e){  System.out.println (e);}
         	 return respuesta; 
    }


/**
* Auto generated method signature
* 
                            * @param ultimaFechaPago 
    * @return ultimaFechaPagoResponse 
*/

        public local.proyectomtis2017.mtis.UltimaFechaPagoResponse ultimaFechaPago
         (
         local.proyectomtis2017.mtis.UltimaFechaPago ultimaFechaPago
         )
   {
       	 Connection connect = null;
         	 Statement statement = null;
         	 UltimaFechaPagoResponse respuesta = new UltimaFechaPagoResponse();
         	 String ped;
         	 try {  	          	
         		 Class.forName("com.mysql.jdbc.Driver");
             	 connect = DriverManager.getConnection(ultimaFechaPago.getBd(), "root", "");
             	 statement = connect.createStatement();   	
             	 ResultSet resultSet = statement.executeQuery("SELECT MAX(FECHA) fecha FROM PAGOS");
                  if (resultSet.next())
                  {
                 	 respuesta.localUltimaFecha = resultSet.getString("fecha");
                      //ped = resultSet.getDate("fecha").toString();
                  }
             	 statement.close();
             	 connect.close();
         	        
         	 } catch (Exception e){  System.out.println (e);}
         	 return respuesta;
     }


/**
* Auto generated method signature
* 
                            * @param totalPedidosRango 
    * @return totalPedidosRangoResponse 
*/

        public local.proyectomtis2017.mtis.TotalPedidosRangoResponse totalPedidosRango
         (
         local.proyectomtis2017.mtis.TotalPedidosRango totalPedidosRango
         )
   {
       	 Connection connect = null;
   	     Statement statement = null;
   	     TotalPedidosRangoResponse respuesta = new TotalPedidosRangoResponse();
   		 try {  	          	
   			 Class.forName("com.mysql.jdbc.Driver");
   			 connect = DriverManager.getConnection(totalPedidosRango.getBd(), "root", "");
   			 statement = connect.createStatement();   	
   	         ResultSet resultSet = statement.executeQuery("select SUM(precioTotal) cantidad from pedidos where fecha >'"+totalPedidosRango.getUltimaFecha()+"'");
              if (resultSet.next())
              {
                	  		respuesta.localPrecioTotalPedidos = resultSet.getFloat("cantidad");
                     }
   	            statement.close();
   	            connect.close();
   	            
   	    } catch (Exception e){  System.out.println (e);}
   		  
   			return respuesta;
      }
     
    }
    