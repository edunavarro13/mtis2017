package org.example.www.local1mtis2017_2;

public class LineaPedido {
	public int id;
	public double precioTotal;
	public int cant;
	public String obs;
	
	public LineaPedido(int id, double precioTotal, int cant, String obs) {
		this.id = id;
		this.precioTotal = precioTotal;
		this.cant = cant;
		this.obs = obs;
	}
	
	public LineaPedido() {

	}
}
