package org.example.www.local1mtis2017_2;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

public class Servicio {
	public String bd;
	public String fecha;
	public double precioTotal;
	public String lineas;
	
	public Servicio(String bd, String fecha, double precioTotal, String lineas) {
		this.bd = bd;
		this.fecha = fecha;
		this.precioTotal = precioTotal;
		this.lineas = lineas;
	}
	
	public Servicio() {
	}
	
	public Servicio fromJson(String json) {
		String[] substr = json.split(",\"linea\":\\[");
		String parte1 = substr[0] + "}";
		String parte2 = substr[1].split("]")[0];
		
		Gson g = new Gson();

		//Deserializacion Primera Parte
		Servicio s = g.fromJson(parte1, Servicio.class);
		s.lineas = parte2;
		return s;
	}
	
	public LineaPedido sacarLineaPedido(String json) {
		LineaPedido x = new LineaPedido();
		
		Gson g2 = new Gson();
		x = g2.fromJson(json, LineaPedido.class);
		
		System.out.println("M�todo a parte" + x.id + "-" + x.precioTotal + "-" + x.cant + "-" + x.obs);
		return x;
	}
}
