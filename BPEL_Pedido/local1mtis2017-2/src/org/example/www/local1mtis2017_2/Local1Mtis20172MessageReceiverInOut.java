
/**
 * Local1Mtis20172MessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package org.example.www.local1mtis2017_2;

        /**
        *  Local1Mtis20172MessageReceiverInOut message receiver
        */

        public class Local1Mtis20172MessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        Local1Mtis20172Skeleton skel = (Local1Mtis20172Skeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("insertarLocal".equals(methodName)){
                
                org.example.www.local1mtis2017_2.InsertarLocalResponse insertarLocalResponse17 = null;
	                        org.example.www.local1mtis2017_2.InsertarLocal wrappedParam =
                                                             (org.example.www.local1mtis2017_2.InsertarLocal)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.www.local1mtis2017_2.InsertarLocal.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               insertarLocalResponse17 =
                                                   
                                                   
                                                         skel.insertarLocal(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), insertarLocalResponse17, false, new javax.xml.namespace.QName("http://www.example.org/local1mtis2017-2/",
                                                    "insertarLocal"));
                                    } else 

            if("insertarGlobal".equals(methodName)){
                
                org.example.www.local1mtis2017_2.InsertarGlobalResponse insertarGlobalResponse19 = null;
	                        org.example.www.local1mtis2017_2.InsertarGlobal wrappedParam =
                                                             (org.example.www.local1mtis2017_2.InsertarGlobal)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.www.local1mtis2017_2.InsertarGlobal.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               insertarGlobalResponse19 =
                                                   
                                                   
                                                         skel.insertarGlobal(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), insertarGlobalResponse19, false, new javax.xml.namespace.QName("http://www.example.org/local1mtis2017-2/",
                                                    "insertarGlobal"));
                                    } else 

            if("insertarLineas".equals(methodName)){
                
                org.example.www.local1mtis2017_2.InsertarLineasResponse insertarLineasResponse21 = null;
	                        org.example.www.local1mtis2017_2.InsertarLineas wrappedParam =
                                                             (org.example.www.local1mtis2017_2.InsertarLineas)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.www.local1mtis2017_2.InsertarLineas.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               insertarLineasResponse21 =
                                                   
                                                   
                                                         skel.insertarLineas(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), insertarLineasResponse21, false, new javax.xml.namespace.QName("http://www.example.org/local1mtis2017-2/",
                                                    "insertarLineas"));
                                    } else 

            if("deserializarPedido".equals(methodName)){
                
                org.example.www.local1mtis2017_2.DeserializarPedidoResponse deserializarPedidoResponse23 = null;
	                        org.example.www.local1mtis2017_2.DeserializarPedido wrappedParam =
                                                             (org.example.www.local1mtis2017_2.DeserializarPedido)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.example.www.local1mtis2017_2.DeserializarPedido.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               deserializarPedidoResponse23 =
                                                   
                                                   
                                                         skel.deserializarPedido(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), deserializarPedidoResponse23, false, new javax.xml.namespace.QName("http://www.example.org/local1mtis2017-2/",
                                                    "deserializarPedido"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarLocal param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarLocal.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarLocalResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarLocalResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarGlobal param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarGlobal.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarGlobalResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarGlobalResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarLineas param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarLineas.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.InsertarLineasResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.InsertarLineasResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.DeserializarPedido param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.DeserializarPedido.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.example.www.local1mtis2017_2.DeserializarPedidoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.example.www.local1mtis2017_2.DeserializarPedidoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.www.local1mtis2017_2.InsertarLocalResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.www.local1mtis2017_2.InsertarLocalResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.www.local1mtis2017_2.InsertarLocalResponse wrapinsertarLocal(){
                                org.example.www.local1mtis2017_2.InsertarLocalResponse wrappedElement = new org.example.www.local1mtis2017_2.InsertarLocalResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.www.local1mtis2017_2.InsertarGlobalResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.www.local1mtis2017_2.InsertarGlobalResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.www.local1mtis2017_2.InsertarGlobalResponse wrapinsertarGlobal(){
                                org.example.www.local1mtis2017_2.InsertarGlobalResponse wrappedElement = new org.example.www.local1mtis2017_2.InsertarGlobalResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.www.local1mtis2017_2.InsertarLineasResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.www.local1mtis2017_2.InsertarLineasResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.www.local1mtis2017_2.InsertarLineasResponse wrapinsertarLineas(){
                                org.example.www.local1mtis2017_2.InsertarLineasResponse wrappedElement = new org.example.www.local1mtis2017_2.InsertarLineasResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.example.www.local1mtis2017_2.DeserializarPedidoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.example.www.local1mtis2017_2.DeserializarPedidoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.example.www.local1mtis2017_2.DeserializarPedidoResponse wrapdeserializarPedido(){
                                org.example.www.local1mtis2017_2.DeserializarPedidoResponse wrappedElement = new org.example.www.local1mtis2017_2.DeserializarPedidoResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (org.example.www.local1mtis2017_2.DeserializarPedido.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.DeserializarPedido.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.DeserializarPedidoResponse.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.DeserializarPedidoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarGlobal.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarGlobal.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarGlobalResponse.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarGlobalResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarLineas.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarLineas.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarLineasResponse.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarLineasResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarLocal.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarLocal.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (org.example.www.local1mtis2017_2.InsertarLocalResponse.class.equals(type)){
                
                        return org.example.www.local1mtis2017_2.InsertarLocalResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    