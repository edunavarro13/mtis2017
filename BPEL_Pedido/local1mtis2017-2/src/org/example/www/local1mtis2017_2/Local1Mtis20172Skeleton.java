package org.example.www.local1mtis2017_2;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import javax.swing.JOptionPane;

import org.example.www.local1mtis2017_2.LineaPedido;
    /**
     *  Local1Mtis20172Skeleton java skeleton for the axisService
     */
    public class Local1Mtis20172Skeleton{
        
         
    	/**
         * Auto generated method signature
         * 
                                     * @param insertarLocal 
             * @return insertarLocalResponse 
         */
        public org.example.www.local1mtis2017_2.InsertarLocalResponse insertarLocal(org.example.www.local1mtis2017_2.InsertarLocal insertarLocal) {
        	org.example.www.local1mtis2017_2.InsertarLocalResponse respuesta = new org.example.www.local1mtis2017_2.InsertarLocalResponse();
          	         	
        	try {	
        		 Class.forName("org.gjt.mm.mysql.Driver");
        	 } catch(Exception e) {
        		 JOptionPane.showMessageDialog(null,e);
        	 }
        	 
    		 String url = "jdbc:mysql://localhost:3306/" + insertarLocal.getBd();
    		 String user = "";
    		 String pass = "";
    		 System.out.println("Conectando...");
    		 try{
    			 Connection cn = DriverManager.getConnection(url,user,pass);
       		 System.out.println("Conectado!!");
    			 java.sql.Connection conect=cn;

    			 if(conect!=null) {
    				PreparedStatement consulta= conect.prepareStatement("INSERT INTO pedidos(fecha,precioTotal) VALUES(?, ?)", Statement.RETURN_GENERATED_KEYS);
    				consulta.setString(1, insertarLocal.getFecha());
    				consulta.setDouble(2, insertarLocal.getPrecioTotal());
    				try{
    					int r = consulta.executeUpdate();
    					ResultSet rs = consulta.getGeneratedKeys();
    					if (rs.next()) {
    					   respuesta.setIdPedido(rs.getInt(1));
    					   respuesta.setRespuesta(true);
    					}

    				}catch(Exception e){
    					System.out.println("Excepci�n: " + e);
    				}
    			 }
    			 cn.close();
    		 }catch(SQLException e){
    			 System.out.println(e.getMessage());
    		 }
    		 return respuesta;
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param insertarGlobal 
             * @return insertarGlobalResponse 
         */
        
         public org.example.www.local1mtis2017_2.InsertarGlobalResponse insertarGlobal(org.example.www.local1mtis2017_2.InsertarGlobal insertarGlobal) {
        	 org.example.www.local1mtis2017_2.InsertarGlobalResponse respuesta = new org.example.www.local1mtis2017_2.InsertarGlobalResponse();
        	 respuesta.setRespuesta(false);
        	 try {	
         		 Class.forName("org.gjt.mm.mysql.Driver");
         	 } catch(Exception e) {
         		 JOptionPane.showMessageDialog(null,e);
         	 }
         	 
     		 String url = "jdbc:mysql://localhost:3306/proyectomtis2017";
     		 String user = "";
     		 String pass = "";
     		 System.out.println("Conectando...");
     		 
        	 int idLocal = -1;
        	 try{
     			 Connection cn = DriverManager.getConnection(url,user,pass);
        		 System.out.println("Conectado!!");
     			 java.sql.Connection conect=cn;

     			 if(conect!=null) {
     				 PreparedStatement consultaSelect = conect.prepareStatement("SELECT id FROM locales WHERE bd=\"jdbc:mysql://localhost:3306/" + insertarGlobal.getBd() + "\"");
     				ResultSet rs = consultaSelect.executeQuery();
 					if (rs.next()) {
 					   idLocal = rs.getInt(1);
 					}
     				
     				PreparedStatement consulta= conect.prepareStatement("INSERT INTO pedidos(descripci�n, total, idLocal) VALUES(?, ?, ?)");
     				consulta.setString(1, "");
     				consulta.setDouble(2, insertarGlobal.getPrecioTotal());
     				consulta.setInt(3, idLocal);
     				try{
     					consulta.executeUpdate();
     					respuesta.setRespuesta(true);
     				}catch(Exception e){
     					System.out.println("Excepci�n: " + e);
     				}
     			 }
     			 cn.close();
     		 }catch(SQLException e){
     			 System.out.println(e.getMessage());
     		 }
        	 return respuesta;
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param insertarLineas 
             * @return insertarLineasResponse 
         */
        
         public org.example.www.local1mtis2017_2.InsertarLineasResponse insertarLineas(org.example.www.local1mtis2017_2.InsertarLineas insertarLineas) {
        	 org.example.www.local1mtis2017_2.InsertarLineasResponse respuesta = new org.example.www.local1mtis2017_2.InsertarLineasResponse();
        	 respuesta.setRespuesta(true);
        	 
        	 String url = "jdbc:mysql://localhost:3306/" + insertarLineas.getBd();
     		 String user = "";
     		 String pass = "";
     		 System.out.println("Conectando...");
     		 
     		String[] lineas1 = insertarLineas.getEntradaJson().split("},");
     		List<LineaPedido> lista = new ArrayList<LineaPedido>();
     		for (int i = 0; i <= lineas1.length-1; i++) {
    			if (i != lineas1.length-1) {
    				lineas1[i] += "}";
    			}
    			System.out.println("paco" + i);
    			System.out.println(lineas1[i]);
    			
    			lista.add(sacarLineaPedido(lineas1[i]));
    		}
     		 
        	 for (int i = 0; i < lista.size();i++) {
        		 try{
         			 Connection cn = DriverManager.getConnection(url,user,pass);
            		 System.out.println("Conectado!!");
         			 java.sql.Connection conect=cn;

         			 if(conect!=null) {
         				PreparedStatement consulta= conect.prepareStatement("INSERT INTO linped(idServicio,cantidad, idPedido, precioLinea, observaciones) VALUES(?, ?, ?, ?, ?)");
         				consulta.setInt(1, lista.get(i).id);
         				consulta.setInt(2, lista.get(i).cant);
         				consulta.setInt(3, insertarLineas.getIdPedido());
         				consulta.setDouble(4, lista.get(i).precioTotal);
         				consulta.setString(5, lista.get(i).obs);
         				try{
         					consulta.executeUpdate();
         				}catch(Exception e){
         					System.out.println("Excepci�n: " + e);
         					respuesta.setRespuesta(false);
         				}
         			 }
         			 cn.close();
         		 }catch(SQLException e){
         			 System.out.println(e.getMessage());
         		 }
        	 }
        	 return respuesta;
        }
         
         public LineaPedido sacarLineaPedido(String json) {
     		LineaPedido x = new LineaPedido();
     		
     		Gson g2 = new Gson();
     		x = g2.fromJson(json, LineaPedido.class);
     		
     		System.out.println("M�todo a parte" + x.id + "-" + x.precioTotal + "-" + x.cant + "-" + x.obs);
     		return x;
     	}
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param deserializarPedido 
             * @return deserializarPedidoResponse 
         */
        
         public org.example.www.local1mtis2017_2.DeserializarPedidoResponse deserializarPedido(org.example.www.local1mtis2017_2.DeserializarPedido deserializarPedido) {
        	 org.example.www.local1mtis2017_2.DeserializarPedidoResponse respuesta = new org.example.www.local1mtis2017_2.DeserializarPedidoResponse();
        	 System.out.println("YEEEEEEEEE");
        	 Servicio x = new Servicio();
        	 x = x.fromJson(deserializarPedido.getJson());
        	 respuesta.setBd(x.bd);
        	 respuesta.setFecha(x.fecha);
        	 respuesta.setPrecioTotal(x.precioTotal);
        	 respuesta.setLinea(x.lineas);
        	 return respuesta;
        }
    }