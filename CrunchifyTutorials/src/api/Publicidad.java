package api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



public class Publicidad {
	
	private String oferta ="na";
	private String fecha ="na";
	private String cadenaXml ="na";
	
	private String codigo ="na";	
	private String idLocal ="na";

	@JsonProperty("oferta")
	public String getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	
	@JsonProperty("fecha")
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	@JsonProperty("cadenaXml")
	public String getCadenaXml() {
		return cadenaXml;
	}
	
	public void setCadenaXml(String cadenaXml) {
		this.cadenaXml = cadenaXml;
	}
	
	
	@JsonProperty("codigo")
	public String getCodigo() {
		return codigo;
	}	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@JsonProperty("idLocal")
	public String getIdLocal() {
		return idLocal;
	}
	public void setIdLocal(String idLocal) {
		this.idLocal = idLocal;
	}
	
	
	

}
