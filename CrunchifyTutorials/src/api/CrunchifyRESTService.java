package api;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 

	@Path("publicidad")
	public class CrunchifyRESTService {
		
		ToolsBD toolsBd = new ToolsBD();
		

		@GET
		@Path("/enviarPublicidad/{idLocal}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response sendPubli(@PathParam("idLocal") String idLocal, InputStream incomingData){
			String result = "GET, servicio ok ";	 
			//Recuperar codPostal de local
			String codigoPostalLocal = toolsBd.getCdLocal(idLocal);
			//recupeara anuncios del local
			List<Publicidad> listaAnuncios = toolsBd.getPubliLocal(idLocal);
 			//Recuperar correos de clientes cercanos
			List<String> correos =  toolsBd.getEmailClientes(codigoPostalLocal);
			//enviar publicidad a todos los emails
	
			GoogleMail emailTools = new GoogleMail();
			for(String correo :correos) {
				
				for(Publicidad anuncio :listaAnuncios) {
					
					//envio de email
					try {
						emailTools.Send("onlyeatmtis", "proyectoMTIS2017", correo, "Oferta en locales cercanos", anuncio.getCadenaXml());
						System.out.println("cooreo mandado");
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
				}				
			}
			
			
			return Response.status(200).entity(result).build();
		}
		
		
		@GET
		@Path("")
		@Produces(MediaType.TEXT_PLAIN)
		public Response getPubliUnLocalOK(InputStream incomingData) {
			String result = "GET, servicio ok ";	 
			// return HTTP response 200 in case of success
			return Response.status(200).entity(result).build();
		}
		
		
		@GET
		@Path("{idLocal}")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public List<Publicidad> getPubliUnLocal(@PathParam("idLocal") String idLocal) {
			String result = "GET for idLocal : " + idLocal; 
			//recuperar de bd
			
			// meter datos
			List<Publicidad> listaPubli = toolsBd.getPubliLocal(idLocal);
			
			//retornar json
			return listaPubli;
		}

		@GET
		@Path("{idLocal}/{idAnuncio}")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Publicidad getPubliUnLocalOK(@PathParam("idLocal") String idLocal,@PathParam("idAnuncio") String idAnuncio) {
			String result = "GET for idLocal : " + idLocal + " iAdnuncio : "+ idAnuncio; 
			//recuperar de bd
			
			// meter datos
			Publicidad p = new Publicidad();
			p = toolsBd.getPubli(idLocal, idAnuncio);
			
			//retornar json
			return p;
		}
		
		
		@POST
		@Path("{idLocal}/{idAnuncio}")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response  postPubliUnLocalOK(@PathParam("idLocal") String idLocal,@PathParam("idAnuncio") String idAnuncio,final Publicidad p) {
			System.out.println(p.getOferta());
			String result = "POST for idLocal : " + idLocal + " iAdnuncio : "+ idAnuncio;
			//Llegan indices y objeto publi
			
			//guardar en bd
			toolsBd.postPubli(idLocal, idAnuncio, p);
			return Response.status(200).entity(result).build();
		}
		
		@PUT
		@Path("{idLocal}/{idAnuncio}")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response putPubliUnLocalOK(@PathParam("idLocal") String idLocal,@PathParam("idAnuncio") String idAnuncio,final Publicidad p) {
			System.out.println("PUT for idLocal : " + idLocal + " iAdnuncio : "+ idAnuncio);
	 
		String	result =  toolsBd.updatePubli(idLocal, idAnuncio, p);
			return Response.status(200).entity(result).build();
		}
		
		@DELETE
		@Path("{idLocal}/{idAnuncio}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response deletePubliUnLocalOK(@PathParam("idLocal") String idLocal,@PathParam("idAnuncio") String idAnuncio,InputStream incomingData) {
			String result = "DELETE for idLocal : " + idLocal + " iAdnuncio : "+ idAnuncio;
			System.out.println("Se ha borrado un anuncio");
			toolsBd.deletePubli(idLocal, idAnuncio);
			return Response.status(200).entity(result).build();
		}
	
	}


	
	

