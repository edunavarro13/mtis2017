﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.Collections;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        private ArrayList locales;
        public Form1()
        {
            InitializeComponent();
        }

        private void boton_Click(object sender, EventArgs e)
        {
            locales = new ArrayList();
            resultado.Text = "";
            var url = "http://localhost:8081/?codp=" + codp.Text;
            ver(url);
            //var local = _download_serialized_json_data<Locales>(url);
        }

        public void ver(string url)
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                //resultado.Text = json_data;
                Char delimiter = '}';
                String[] substrings = json_data.Split(delimiter);
                for(int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";
                    agregarLocales(substrings[i]);

                }
                String ciudades = "";
                foreach (Locales local in locales)
                    ciudades += local.nombre + "\r\n";
                resultado.Text = ciudades;

            }
        }

        public void agregarLocales(string json)
        {
            Locales l = JsonConvert.DeserializeObject<Locales>(json);
            locales.Add(l);
        }

        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}
