﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clienteMTIS2017
{
    class serviciosLocal
    {
        public string nombre { get; set; }
        public double precio { get; set; }
        public string descripcion { get; set; }
        public string tipo { get; set; }
        public int id { get; set; }
    }
}
