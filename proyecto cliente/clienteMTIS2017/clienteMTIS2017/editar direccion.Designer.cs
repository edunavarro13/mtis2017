﻿namespace clienteMTIS2017
{
    partial class editar_direccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.provincia = new System.Windows.Forms.TextBox();
            this.poblacion = new System.Windows.Forms.TextBox();
            this.cp = new System.Windows.Forms.TextBox();
            this.direccion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.direccionDatos = new System.Windows.Forms.Label();
            this.cpDatos = new System.Windows.Forms.Label();
            this.poblacionDatos = new System.Windows.Forms.Label();
            this.provinciaDatos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(38, 296);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 46);
            this.button1.TabIndex = 17;
            this.button1.Text = "EDITAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // provincia
            // 
            this.provincia.Location = new System.Drawing.Point(430, 222);
            this.provincia.Name = "provincia";
            this.provincia.Size = new System.Drawing.Size(329, 20);
            this.provincia.TabIndex = 16;
            // 
            // poblacion
            // 
            this.poblacion.Location = new System.Drawing.Point(430, 156);
            this.poblacion.Name = "poblacion";
            this.poblacion.Size = new System.Drawing.Size(329, 20);
            this.poblacion.TabIndex = 15;
            // 
            // cp
            // 
            this.cp.Location = new System.Drawing.Point(430, 86);
            this.cp.Name = "cp";
            this.cp.Size = new System.Drawing.Size(329, 20);
            this.cp.TabIndex = 14;
            // 
            // direccion
            // 
            this.direccion.Location = new System.Drawing.Point(430, 29);
            this.direccion.Name = "direccion";
            this.direccion.Size = new System.Drawing.Size(329, 20);
            this.direccion.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "PROVINCIA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "POBLACION";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "CODIGO POSTAL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "DIRECCION";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(308, 296);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(208, 46);
            this.button2.TabIndex = 18;
            this.button2.Text = "BORRAR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // direccionDatos
            // 
            this.direccionDatos.AutoSize = true;
            this.direccionDatos.Location = new System.Drawing.Point(185, 35);
            this.direccionDatos.Name = "direccionDatos";
            this.direccionDatos.Size = new System.Drawing.Size(35, 13);
            this.direccionDatos.TabIndex = 19;
            this.direccionDatos.Text = "label5";
            // 
            // cpDatos
            // 
            this.cpDatos.AutoSize = true;
            this.cpDatos.Location = new System.Drawing.Point(188, 92);
            this.cpDatos.Name = "cpDatos";
            this.cpDatos.Size = new System.Drawing.Size(35, 13);
            this.cpDatos.TabIndex = 20;
            this.cpDatos.Text = "label5";
            // 
            // poblacionDatos
            // 
            this.poblacionDatos.AutoSize = true;
            this.poblacionDatos.Location = new System.Drawing.Point(188, 162);
            this.poblacionDatos.Name = "poblacionDatos";
            this.poblacionDatos.Size = new System.Drawing.Size(35, 13);
            this.poblacionDatos.TabIndex = 21;
            this.poblacionDatos.Text = "label5";
            // 
            // provinciaDatos
            // 
            this.provinciaDatos.AutoSize = true;
            this.provinciaDatos.Location = new System.Drawing.Point(191, 228);
            this.provinciaDatos.Name = "provinciaDatos";
            this.provinciaDatos.Size = new System.Drawing.Size(35, 13);
            this.provinciaDatos.TabIndex = 22;
            this.provinciaDatos.Text = "label5";
            // 
            // editar_direccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 419);
            this.Controls.Add(this.provinciaDatos);
            this.Controls.Add(this.poblacionDatos);
            this.Controls.Add(this.cpDatos);
            this.Controls.Add(this.direccionDatos);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.provincia);
            this.Controls.Add(this.poblacion);
            this.Controls.Add(this.cp);
            this.Controls.Add(this.direccion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "editar_direccion";
            this.Text = "editar_direccion";
            this.Load += new System.EventHandler(this.editar_direccion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox provincia;
        private System.Windows.Forms.TextBox poblacion;
        private System.Windows.Forms.TextBox cp;
        private System.Windows.Forms.TextBox direccion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label direccionDatos;
        private System.Windows.Forms.Label cpDatos;
        private System.Windows.Forms.Label poblacionDatos;
        private System.Windows.Forms.Label provinciaDatos;
    }
}