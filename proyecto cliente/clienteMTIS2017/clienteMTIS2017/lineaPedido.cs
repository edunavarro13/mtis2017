﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clienteMTIS2017
{
    class lineaPedido
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public double precioTotal { get; set; }
        public int cantidad { get; set; }
        public string observaciones { get; set; }
    }
}
