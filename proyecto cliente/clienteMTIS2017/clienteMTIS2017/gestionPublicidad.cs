﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class gestionPublicidad : Form
    {

        public Form1 padre;
        private ArrayList listaAnuncios;
        private Anuncio aux ;
        private String idLocal = "";
        static HttpClient client = new HttpClient();


        public gestionPublicidad()
        {
            InitializeComponent();
            listBox1.Items.Clear();
            textBox1.Clear();
            client.BaseAddress = new Uri("http://localhost:7080/CrunchifyTutorials/api/publicidad/");
        }

    

        

        /// //////////////////////////////////////////////////////////////////



        private void button1_Click(object sender, EventArgs e)
        {
            operationClickButton1();
        }

        private void operationClickButton1()
        {
            //Recuperar y mostrar lista de anuncios
            //URL para hacer GET de anuncios
            listaAnuncios = new ArrayList();
            listBox1.Items.Clear();
            var url = "http://localhost:7080/CrunchifyTutorials/api/publicidad/" + textBox1.Text;
            idLocal = textBox1.Text;
            ver_codigos_postales(url);

        }



        public void ver_codigos_postales(string url)
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                //resultado.Text = json_data;
                Char delimiter = '}';
                String[] substrings = json_data.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";
                    agregarAnuncio(substrings[i]);

                }
                foreach (Anuncio anuncio in listaAnuncios)
                {
                    listBox1.Items.Add(anuncio.oferta);
                }

            }
        }

        public void agregarAnuncio(string json)
        {
            Anuncio l = JsonConvert.DeserializeObject<Anuncio>(json);
            listaAnuncios.Add(l);
        }

        private void seleccionarAnuncio_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && !padre.listarPlatos)
            {
                Anuncio auxloc = (Anuncio)listaAnuncios[listBox1.SelectedIndex];
                richTextBox1.Text = auxloc.cadenaXml;
                textBox2.Text = auxloc.codigo;
                textBox3.Text = auxloc.oferta;
                textBox4.Text = auxloc.fecha;

                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            //Recoger datos de la vista
            //Crear
            //hacer POST de anuncio

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
           

            Anuncio auxloc = new Anuncio();
            auxloc.cadenaXml = richTextBox1.Text;
            auxloc.codigo = textBox2.Text;
            auxloc.oferta = textBox3.Text;
            auxloc.fecha = textBox4.Text;

            var stringContent = new StringContent(JsonConvert.SerializeObject(auxloc), Encoding.UTF8, "application/json");


            // List data response.
            HttpResponseMessage response = client.PostAsync(textBox1.Text + "/" + textBox2.Text, stringContent).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                operationClickButton1();
                richTextBox1.Text = "Anuncio insertado";
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            Anuncio auxloc = new Anuncio();
            auxloc.cadenaXml = richTextBox1.Text;
            auxloc.codigo = textBox2.Text;
            auxloc.oferta = textBox3.Text;
            auxloc.fecha = textBox4.Text;

            var stringContent = new StringContent(JsonConvert.SerializeObject(auxloc), Encoding.UTF8, "application/json");


            // List data response.
            HttpResponseMessage response = client.PutAsync(idLocal + "/" + textBox2.Text, stringContent).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {

                operationClickButton1();
                richTextBox1.Text = "Anuncio modificado";

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }



        }


        private void button4_Click(object sender, EventArgs e)
        {

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.DeleteAsync(idLocal+"/"+textBox2.Text).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {


                operationClickButton1();
                richTextBox1.Text = "Anuncio borrado";

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

        }



        private void button5_Click(object sender, EventArgs e)
        {

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("enviarPublicidad/" +idLocal ).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {

                operationClickButton1();
                richTextBox1.Text = "Anuncios enviados";

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

        }




        //////////////////////////////////////////////////////////////////////
        private void gestionPublicidad_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
