﻿namespace clienteMTIS2017
{
    partial class gestionCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gestionCuenta));
            this.metodospago = new System.Windows.Forms.ListBox();
            this.direcciones = new System.Windows.Forms.ListBox();
            this.titulo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nombre = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.Label();
            this.cp = new System.Windows.Forms.Label();
            this.telefono = new System.Windows.Forms.Label();
            this.editarnombre = new System.Windows.Forms.TextBox();
            this.editaremail = new System.Windows.Forms.TextBox();
            this.editarcp = new System.Windows.Forms.TextBox();
            this.editartelefono = new System.Windows.Forms.TextBox();
            this.editarButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.editarpass = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // metodospago
            // 
            this.metodospago.FormattingEnabled = true;
            this.metodospago.Location = new System.Drawing.Point(484, 279);
            this.metodospago.Name = "metodospago";
            this.metodospago.Size = new System.Drawing.Size(440, 355);
            this.metodospago.TabIndex = 1;
            this.metodospago.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // direcciones
            // 
            this.direcciones.FormattingEnabled = true;
            this.direcciones.Location = new System.Drawing.Point(13, 279);
            this.direcciones.Name = "direcciones";
            this.direcciones.Size = new System.Drawing.Size(449, 355);
            this.direcciones.TabIndex = 2;
            this.direcciones.SelectedIndexChanged += new System.EventHandler(this.direcciones_SelectedIndexChanged);
            // 
            // titulo
            // 
            this.titulo.AutoSize = true;
            this.titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titulo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.titulo.Location = new System.Drawing.Point(12, 9);
            this.titulo.Name = "titulo";
            this.titulo.Size = new System.Drawing.Size(63, 13);
            this.titulo.TabIndex = 3;
            this.titulo.Text = "USUARIO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "NOMBRE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "EMAIL:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "CODIGO POSTAL:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "TELEFONO:";
            // 
            // nombre
            // 
            this.nombre.AutoSize = true;
            this.nombre.Location = new System.Drawing.Point(124, 42);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(35, 13);
            this.nombre.TabIndex = 8;
            this.nombre.Text = "label5";
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Location = new System.Drawing.Point(124, 72);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(35, 13);
            this.email.TabIndex = 9;
            this.email.Text = "label5";
            // 
            // cp
            // 
            this.cp.AutoSize = true;
            this.cp.Location = new System.Drawing.Point(124, 102);
            this.cp.Name = "cp";
            this.cp.Size = new System.Drawing.Size(35, 13);
            this.cp.TabIndex = 10;
            this.cp.Text = "label5";
            // 
            // telefono
            // 
            this.telefono.AutoSize = true;
            this.telefono.Location = new System.Drawing.Point(124, 132);
            this.telefono.Name = "telefono";
            this.telefono.Size = new System.Drawing.Size(35, 13);
            this.telefono.TabIndex = 11;
            this.telefono.Text = "label5";
            // 
            // editarnombre
            // 
            this.editarnombre.Location = new System.Drawing.Point(239, 42);
            this.editarnombre.Name = "editarnombre";
            this.editarnombre.Size = new System.Drawing.Size(152, 20);
            this.editarnombre.TabIndex = 12;
            // 
            // editaremail
            // 
            this.editaremail.Location = new System.Drawing.Point(239, 72);
            this.editaremail.Name = "editaremail";
            this.editaremail.Size = new System.Drawing.Size(152, 20);
            this.editaremail.TabIndex = 13;
            // 
            // editarcp
            // 
            this.editarcp.Location = new System.Drawing.Point(239, 102);
            this.editarcp.Name = "editarcp";
            this.editarcp.Size = new System.Drawing.Size(152, 20);
            this.editarcp.TabIndex = 14;
            // 
            // editartelefono
            // 
            this.editartelefono.Location = new System.Drawing.Point(239, 132);
            this.editartelefono.Name = "editartelefono";
            this.editartelefono.Size = new System.Drawing.Size(152, 20);
            this.editartelefono.TabIndex = 15;
            // 
            // editarButton
            // 
            this.editarButton.Location = new System.Drawing.Point(239, 171);
            this.editarButton.Name = "editarButton";
            this.editarButton.Size = new System.Drawing.Size(152, 27);
            this.editarButton.TabIndex = 16;
            this.editarButton.Text = "EDITAR DATOS";
            this.editarButton.UseVisualStyleBackColor = true;
            this.editarButton.Click += new System.EventHandler(this.editarButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(636, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 24);
            this.label5.TabIndex = 17;
            this.label5.Text = "Cambiar contraseña";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(596, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "CONTRASEÑA NUEVA:";
            // 
            // editarpass
            // 
            this.editarpass.Location = new System.Drawing.Point(599, 102);
            this.editarpass.Name = "editarpass";
            this.editarpass.PasswordChar = '*';
            this.editarpass.Size = new System.Drawing.Size(215, 20);
            this.editarpass.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(599, 132);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 32);
            this.button1.TabIndex = 20;
            this.button1.Text = "CAMBIAR CONTRASEÑA";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "DIRECCIONES";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(484, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "METODOS DE PAGO";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(127, 244);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 27);
            this.button2.TabIndex = 23;
            this.button2.Text = "NUEVA DIRECCION";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(631, 244);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(148, 26);
            this.button3.TabIndex = 24;
            this.button3.Text = "NUEVO METODO";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // gestionCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 646);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.editarpass);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.editarButton);
            this.Controls.Add(this.editartelefono);
            this.Controls.Add(this.editarcp);
            this.Controls.Add(this.editaremail);
            this.Controls.Add(this.editarnombre);
            this.Controls.Add(this.telefono);
            this.Controls.Add(this.cp);
            this.Controls.Add(this.email);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titulo);
            this.Controls.Add(this.direcciones);
            this.Controls.Add(this.metodospago);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "gestionCuenta";
            this.Text = "gestionCuenta";
            this.Load += new System.EventHandler(this.gestionCuenta_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox metodospago;
        private System.Windows.Forms.ListBox direcciones;
        private System.Windows.Forms.Label titulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label nombre;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.Label cp;
        private System.Windows.Forms.Label telefono;
        private System.Windows.Forms.TextBox editarnombre;
        private System.Windows.Forms.TextBox editaremail;
        private System.Windows.Forms.TextBox editarcp;
        private System.Windows.Forms.TextBox editartelefono;
        private System.Windows.Forms.Button editarButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox editarpass;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}