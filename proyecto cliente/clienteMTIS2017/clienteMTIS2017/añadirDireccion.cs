﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class añadirDireccion : Form
    {
        public gestionCuenta padre;
        public string username;
        public añadirDireccion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(direccion.Text!="" & cp.Text != "" & poblacion.Text != "" & provincia.Text != "")
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:8081/anyadirdireccion");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"usuario\":\""+username+"\",\"direccion\":\""+direccion.Text+"\",\"cp\":\""+cp.Text+"\",\"poblacion\":\""+poblacion.Text+"\",\"ciudad\":\""+provincia.Text+"\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                try
                {
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                    StringBuilder output = new StringBuilder();
                    output.Append(reader.ReadToEnd());
                    httpResponse.Close();
                    string jsonResultado = output.ToString();
                    if (jsonResultado == "1")
                    {
                        string message = "Ha añadido correctamente su direccion";
                        string caption = "Exito";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;

                        DialogResult result = MessageBox.Show(message, caption, buttons);
                        if(result == System.Windows.Forms.DialogResult.OK)
                        {
                            this.Close();
                            padre.ver_direcciones("http://localhost:8081/leerdirecciones");
                            padre.Refresh();
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                string message = "Todos los campos deben ser rellenados para poder continuar";
                string caption = "Campos vacios";
                MessageBoxButtons buttons = MessageBoxButtons.OK;

                DialogResult result = MessageBox.Show(message, caption, buttons);
            }
        }
    }
}
