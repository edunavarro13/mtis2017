﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class Form1 : Form
    {
        Login login;
        listarLocales listar;
        listarPlatos listarP;
        gestionCuenta adminCuenta;
        gestionPublicidad gestionP;
        Menu menu;

        public string bdLocalConsultar;
        public string localConsultar;

        public bool usuarioIniciado = true;
        
        public bool listarLocales = false;
        public bool listarPlatos = false;
        public bool miCuenta = false;

        public string username;
        public Form1()
        {
            InitializeComponent();
            username = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            menuStrip1.Visible = false;

            login = new Login();
            login.padre = this;
            login.MdiParent = this;
            login.Show();
        }

        public void hacerPedidoToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!listarLocales)
            {
                listarLocales = true;
                listar = new listarLocales();
                listar.padre = this;
                listar.MdiParent = this;
                listar.Show();
            } else
            {
                listar.Focus();
            }
        }
        public void setUsername(string user)
        {
            username = user;
        }

        public void administrarPublicidadToolStripMenuItem_Click(object sender, EventArgs e) {

            gestionP = new gestionPublicidad();
            gestionP.padre = this;
            gestionP.MdiParent = this;
            gestionP.Show();

        }

        public void administrarCuentaToolStripMenuItem_Click(object sender, EventArgs e) {
            miCuenta = true;
            adminCuenta = new gestionCuenta();
            adminCuenta.padre = this;
            adminCuenta.MdiParent = this;
            adminCuenta.username = username;
            adminCuenta.Show();

        }

        public void abrirListarPlatos()
        {
            listarPlatos = true;
            listarP = new listarPlatos();
            listarP.padre = this;
            listarP.MdiParent = this;
            listarP.Show();
        }

        public void abrirMenu()
        {
            menu = new Menu();
            menu.padre = this;
            menu.MdiParent = this;
            menu.Show();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e) {
            if (listarLocales)
                listar.Close();
            if (listarPlatos)
                listarP.Close();

            usuarioIniciado = true;
            username = null;
            login = new Login();
            login.padre = this;
            login.MdiParent = this;
            login.Show();
            menu.Close();

            menuStrip1.Visible = false;
        }
    }
}
