﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class Registro : Form
    {

        public Login padre;

        public Registro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //recoger datos

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (pass1.Text.Equals(pass2.Text))
            {
                error.Visible = false;
                error.Text = "Los campos de contraseña han de ser iguales.";
                try
                {
                    var url = "http://localhost:8081/registrarUsuario?phone=" + phone.Text + "&&user=" + username.Text + "&&pass=" + pass1.Text + "&&name=" + name.Text + "&&email=" + email.Text + "&&cp=" + cp.Text + "";
                    using (var w = new WebClient())
                    {
                        var json_data = string.Empty;
                        // attempt to download JSON data as a string
                        try
                        {
                            json_data = w.DownloadString(url);
                            MessageBox.Show("Se ha creado el usuario " + username.Text, "Usuario creado con exito", MessageBoxButtons.OK);
                            this.Close();
                        }
                        catch (Exception)
                        {
                            error.Text = "El usuario ya existe en la base de datos.";
                            error.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Excepción producida", ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                error.Text = "Los campos de contraseña han de ser iguales.";
                error.Visible = true;
            }
        }
    }
}
