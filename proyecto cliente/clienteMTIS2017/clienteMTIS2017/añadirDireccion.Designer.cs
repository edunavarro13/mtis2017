﻿namespace clienteMTIS2017
{
    partial class añadirDireccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(añadirDireccion));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.direccion = new System.Windows.Forms.TextBox();
            this.cp = new System.Windows.Forms.TextBox();
            this.poblacion = new System.Windows.Forms.TextBox();
            this.provincia = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DIRECCION";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "CODIGO POSTAL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "POBLACION";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "PROVINCIA";
            // 
            // direccion
            // 
            this.direccion.Location = new System.Drawing.Point(175, 35);
            this.direccion.Name = "direccion";
            this.direccion.Size = new System.Drawing.Size(329, 20);
            this.direccion.TabIndex = 4;
            // 
            // cp
            // 
            this.cp.Location = new System.Drawing.Point(175, 85);
            this.cp.Name = "cp";
            this.cp.Size = new System.Drawing.Size(329, 20);
            this.cp.TabIndex = 5;
            // 
            // poblacion
            // 
            this.poblacion.Location = new System.Drawing.Point(175, 155);
            this.poblacion.Name = "poblacion";
            this.poblacion.Size = new System.Drawing.Size(329, 20);
            this.poblacion.TabIndex = 6;
            // 
            // provincia
            // 
            this.provincia.Location = new System.Drawing.Point(175, 221);
            this.provincia.Name = "provincia";
            this.provincia.Size = new System.Drawing.Size(329, 20);
            this.provincia.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(175, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 46);
            this.button1.TabIndex = 8;
            this.button1.Text = "AÑADIR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // añadirDireccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 426);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.provincia);
            this.Controls.Add(this.poblacion);
            this.Controls.Add(this.cp);
            this.Controls.Add(this.direccion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "añadirDireccion";
            this.Text = "añadirDireccion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox direccion;
        private System.Windows.Forms.TextBox cp;
        private System.Windows.Forms.TextBox poblacion;
        private System.Windows.Forms.TextBox provincia;
        private System.Windows.Forms.Button button1;
    }
}