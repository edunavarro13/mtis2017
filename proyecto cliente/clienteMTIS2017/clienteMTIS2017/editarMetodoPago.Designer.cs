﻿namespace clienteMTIS2017
{
    partial class editarMetodoPago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(editarMetodoPago));
            this.valorDatos = new System.Windows.Forms.Label();
            this.tipoDatos = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.valor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tipo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // valorDatos
            // 
            this.valorDatos.AutoSize = true;
            this.valorDatos.Location = new System.Drawing.Point(171, 134);
            this.valorDatos.Name = "valorDatos";
            this.valorDatos.Size = new System.Drawing.Size(35, 13);
            this.valorDatos.TabIndex = 30;
            this.valorDatos.Text = "label5";
            // 
            // tipoDatos
            // 
            this.tipoDatos.AutoSize = true;
            this.tipoDatos.Location = new System.Drawing.Point(171, 33);
            this.tipoDatos.Name = "tipoDatos";
            this.tipoDatos.Size = new System.Drawing.Size(35, 13);
            this.tipoDatos.TabIndex = 29;
            this.tipoDatos.Text = "label5";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(359, 243);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(208, 46);
            this.button2.TabIndex = 28;
            this.button2.Text = "BORRAR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(29, 243);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 46);
            this.button1.TabIndex = 27;
            this.button1.Text = "EDITAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // valor
            // 
            this.valor.Location = new System.Drawing.Point(359, 127);
            this.valor.Name = "valor";
            this.valor.Size = new System.Drawing.Size(329, 20);
            this.valor.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "DATOS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "TIPO";
            // 
            // tipo
            // 
            this.tipo.FormattingEnabled = true;
            this.tipo.Items.AddRange(new object[] {
            "Tarjeta",
            "Paypal"});
            this.tipo.Location = new System.Drawing.Point(359, 33);
            this.tipo.Name = "tipo";
            this.tipo.Size = new System.Drawing.Size(329, 21);
            this.tipo.TabIndex = 31;
            // 
            // editarMetodoPago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 413);
            this.Controls.Add(this.tipo);
            this.Controls.Add(this.valorDatos);
            this.Controls.Add(this.tipoDatos);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.valor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "editarMetodoPago";
            this.Text = "editarMetodoPago";
            this.Load += new System.EventHandler(this.editarMetodoPago_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label valorDatos;
        private System.Windows.Forms.Label tipoDatos;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox valor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox tipo;
    }
}