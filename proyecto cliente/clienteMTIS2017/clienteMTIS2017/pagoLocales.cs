﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class pagoLocales : Form
    {
        public Form1 padre;
        List<localPago> listaLocales = new List<localPago>();
        DialogResult result;
        public string username;
        public pagoLocales()
        {
            InitializeComponent();
        }

        public void ver_locales(string url)
        {
            listaLocales.Clear();
            locales.Items.Clear();
          
            //procedo a listar todos los platos del local seleccionado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"usuario\":\"" + username + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";

                    localPago sl = JsonConvert.DeserializeObject<localPago>(substrings[i]);
                    listaLocales.Add(sl);
                }
                foreach (localPago local in listaLocales)
                {
                    locales.Items.Add(local.id + ", " + local.nombre);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void locales_SelectedIndexChanged(object sender, EventArgs e)
        {
            localPago auxloc = listaLocales[locales.SelectedIndex];
            //process
        }
    }
}
