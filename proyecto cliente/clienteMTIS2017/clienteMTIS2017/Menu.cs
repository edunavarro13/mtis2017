﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class Menu : Form {
        public Form1 padre;
        public listarLocales pedido;
        public Menu() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            padre.hacerPedidoToolStripMenuItem_Click(sender, e);
        }

        private void button2_Click(object sender, EventArgs e) {
            padre.administrarPublicidadToolStripMenuItem_Click(sender, e);
        }

        private void button3_Click(object sender, EventArgs e) {
            padre.administrarCuentaToolStripMenuItem_Click(sender, e);
        }
    }
}
