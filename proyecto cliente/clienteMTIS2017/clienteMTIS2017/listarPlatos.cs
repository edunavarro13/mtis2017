﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class listarPlatos : Form
    {
        public Form1 padre;
        List<serviciosLocal> listaServicios = new List<serviciosLocal>();
        List<lineaPedido> listaLineasPedido = new List<lineaPedido>();
        public listarPlatos()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                int posicion = listBox1.SelectedIndex;
                serviciosLocal s = listaServicios[posicion];
                lineaPedido lp = new lineaPedido();

                lp.id = s.id;
                lp.precioTotal = s.precio * Int32.Parse(textBox1.Text);
                lp.cantidad = Int32.Parse(textBox1.Text);
                lp.observaciones = textBox2.Text;
                lp.nombre = s.nombre;

                listaLineasPedido.Add(lp);
                rellenarListBox(listaLineasPedido);

                textBox1.Text = "1";
                textBox2.Text = "Ninguna";
            }
        }

        private void rellenarListBox(List<lineaPedido> l)
        {
            listBox2.Items.Clear();
            for (int i = 0; i < l.Count; i++)
            {
                listBox2.Items.Add("NOMBRE: " + l[i].nombre + ", CANTIDAD: " + l[i].cantidad + ", OBSERVACIONES: " + l[i].observaciones + ", PRECIO: " + l[i].precioTotal + "€");
            }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            label3.Visible = true;
            textBox1.Visible = true;
            label4.Visible = true;
            textBox2.Visible = true;

        }

        private void listarPlatos_Load(object sender, EventArgs e)
        {
            //procedo a listar todos los platos del local seleccionado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:8081/listarPlatos");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = padre.bdLocalConsultar;

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";

                    serviciosLocal sl = JsonConvert.DeserializeObject<serviciosLocal>(substrings[i]);
                    listaServicios.Add(sl);
                }
                foreach (serviciosLocal servlocal in listaServicios)
                {
                    listBox1.Items.Add("NOMBRE: " + servlocal.nombre + ", DESCRIPCIÓN: " + servlocal.descripcion + ", PRECIO: " + servlocal.precio + "€");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                int posicion = listBox2.SelectedIndex;
                listaLineasPedido.Remove(listaLineasPedido[posicion]);
                rellenarListBox(listaLineasPedido);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string fecha = DateTime.Now.ToString();
            string[] subFecha = fecha.Split(' ');
            string fechaBuena = subFecha[0].Split('/')[2] + "-" + subFecha[0].Split('/')[1] + "-" + subFecha[0].Split('/')[0];

            string jsonHacerPedido = "{";

            double precioTotal = 0;
            string productos = "";

            for (int i = 0; i < listaLineasPedido.Count; i++)
            {
                precioTotal += listaLineasPedido[i].precioTotal;
                productos += "{" + "\"id\":" + "\"" + listaLineasPedido[i].id + "\"," + "\"precioTotal\":" + "\"" + listaLineasPedido[i].precioTotal.ToString().Replace(",", ".") + "\"," + "\"cant\":" + "\"" + listaLineasPedido[i].cantidad + "\"," + "\"obs\":" + "\"" + listaLineasPedido[i].observaciones + "\"}";
                if (i != listaLineasPedido.Count - 1)
                    productos += ",";
                else
                    productos += "]";
            }

            jsonHacerPedido += "\"bd\":\"" + padre.localConsultar + "\",\"fecha\":\"" + fechaBuena + "\",\"precioTotal\":\"" + precioTotal.ToString().Replace(",", ".") + "\"," + "\"linea\":[" + productos + "}";

            //MessageBox.Show(jsonHacerPedido);

            //procedo a listar todos los platos del local seleccionado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:8081/hacerPed");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonHacerPedido);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Pedido realizado");
            }
            this.Close();
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            button2_Click(sender, e);
        }

        private void listarPlatos_FormClosed(object sender, FormClosedEventArgs e)
        {
            padre.listarPlatos = false;
        }
    }
}
