﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class editar_direccion : Form
    {
        public address objetivo;
        public string username;
        internal gestionCuenta padre;

        public editar_direccion()
        {
            InitializeComponent();
        }

        public void editarDireccion(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            if (direccion.Text != "")
                objetivo.direccion = direccion.Text;
            if (cp.Text != "")
                objetivo.cp = cp.Text;
            if (poblacion.Text != "")
                objetivo.poblacion = poblacion.Text;
            if (provincia.Text != "")
                objetivo.ciudad = provincia.Text;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(objetivo);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();

                if (jsonResultado == "1")
                {
                    string message = "Ha editado correctamente su direccion";
                    string caption = "Exito";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;

                    DialogResult result = MessageBox.Show(message, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                        padre.ver_direcciones("http://localhost:8081/leerdirecciones");
                        padre.Refresh();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void borrarDireccion(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"id\":\""+objetivo.id+"\",\"usuario\":\"" + username +"\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();

                if (jsonResultado == "1")
                {
                    string message = "Ha borrado correctamente su direccion";
                    string caption = "Exito";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;

                    DialogResult result = MessageBox.Show(message, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                        padre.ver_direcciones("http://localhost:8081/leerdirecciones");
                        padre.Refresh();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            editarDireccion("http://localhost:8081/editardireccion");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            borrarDireccion("http://localhost:8081/borrardireccion");
        }

        private void editar_direccion_Load(object sender, EventArgs e)
        {
            direccionDatos.Text = objetivo.direccion;
            cpDatos.Text = objetivo.cp;
            poblacionDatos.Text = objetivo.poblacion;
            provinciaDatos.Text = objetivo.ciudad;
        }
    }
}
