﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class editarMetodoPago : Form
    {
        public metodoPago objetivo;
        public string username;
        internal gestionCuenta padre;
        public editarMetodoPago()
        {
            InitializeComponent();
        }

        public void editarMetodo(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            if (tipo.Text != "")
                objetivo.tipo = tipo.Text;
            if (valor.Text != "")
                objetivo.valor = valor.Text;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(objetivo);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();

                if (jsonResultado == "1")
                {
                    string message = "Ha editado correctamente su metodo de pago";
                    string caption = "Exito";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;

                    DialogResult result = MessageBox.Show(message, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                        padre.ver_metodospago("http://localhost:8081/leermetodospago");
                        padre.Refresh();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void borrarMetodo(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"id\":\"" + objetivo.id +"\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();

                if (jsonResultado == "1")
                {
                    string message = "Ha borrado correctamente su metodo de pago";
                    string caption = "Exito";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;

                    DialogResult result = MessageBox.Show(message, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                        padre.ver_metodospago("http://localhost:8081/leermetodospago");
                        padre.Refresh();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            editarMetodo("http://localhost:8081/editarmetodopago");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            borrarMetodo("http://localhost:8081/borrarmetodopago");
        }

        private void editarMetodoPago_Load(object sender, EventArgs e)
        {
            tipoDatos.Text = objetivo.tipo;
            valorDatos.Text = objetivo.valor;
        }
    }
}
