﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class Login : Form {
        public Form1 padre;
        public Menu menu;
        private Registro registro;


        public Login() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            var url = "http://localhost:8081/login?user=" + textBox1.Text + "&&pass=" + textBox2.Text;
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                    int tam = json_data.Length;
                    // Obtiene 2 si no se encuentra, si se encuentra devuelve minimo 10
                    if (tam > 2)
                    {
                        error.Visible = false;
                        padre.usuarioIniciado = false;
                        padre.username = textBox1.Text;
                        padre.abrirMenu();

                        MenuStrip control = (MenuStrip)this.MdiParent.Controls["menuStrip1"];
                        control.Visible = true;

                        this.Close();
                    }
                    else
                        error.Visible = true;
                }
                catch (Exception) { }
            }
        }


        private void link1_Click(object sender, EventArgs e)
        {
            registro = new Registro();
            registro.padre = this;
            registro.MdiParent = padre;
            registro.Show();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
    }
}
