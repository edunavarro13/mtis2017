﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class gestionCuenta : Form
    {
        public Form1 padre;
        private perfilUsuario user;
        List<address> listaDirecciones = new List<address>();
        List<metodoPago> listaMetodosPago = new List<metodoPago>();
        DialogResult result;
        public string username;
        public añadirDireccion nuevaDireccion;
        public editar_direccion editarDireccion;
        public añadirMetodoPago nuevoMetodo;
        public editarMetodoPago editarMetodo;
        public gestionCuenta()
        {
            username = "";
            InitializeComponent();

        }

        public void ver_perfil(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"usuario\":\""+ username + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                String aux = substrings[0].Substring(1);
                substrings[0] = aux + "}";
                user = new perfilUsuario();
                user = JsonConvert.DeserializeObject<perfilUsuario>(substrings[0]);
                nombre.Text = user.nombre;
                cp.Text = user.cp;
                telefono.Text = user.telefono;
                email.Text = user.email;
                editarcp.Text = "";
                editarnombre.Text = "";
                editartelefono.Text = "";
                editaremail.Text = "";
                editarpass.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ver_direcciones(string url)
        {
            listaDirecciones.Clear();
            direcciones.Items.Clear();
            //procedo a listar todos los platos del local seleccionado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"usuario\":\"" + username + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";

                    address sl = JsonConvert.DeserializeObject<address>(substrings[i]);
                    listaDirecciones.Add(sl);
                }
                foreach (address local in listaDirecciones)
                {
                    direcciones.Items.Add(listaDirecciones.IndexOf(local)+1 + ", " + local.direccion + ", " + local.poblacion + ", " + local.cp + ", " + local.ciudad);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ver_metodospago(string url)
        {
            listaMetodosPago.Clear();
            metodospago.Items.Clear();
            //procedo a listar todos los platos del local seleccionado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"usuario\":\"" + username + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";

                    metodoPago sl = JsonConvert.DeserializeObject<metodoPago>(substrings[i]);
                    listaMetodosPago.Add(sl);
                }
                foreach (metodoPago local in listaMetodosPago)
                {
                    metodospago.Items.Add(listaMetodosPago.IndexOf(local)+1 + ", " + local.tipo + ": " + local.valor);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void editar_perfil(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            user.usuario = username;
            if (editarnombre.Text != "")
                user.nombre = editarnombre.Text;
            if (editarcp.Text != "")
                user.cp = editarcp.Text;
            if (editaremail.Text != "")
                user.email = editaremail.Text;
            if (editartelefono.Text != "")
                user.telefono = editartelefono.Text;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(user);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ver_perfil("http://localhost:8081/leerdatos");
            this.Refresh();
        }


        public void editar_contraseña(string url)
        {
            if (editarpass.Text != "")
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string json = "{\"usuario\":\"" + username + "\",\"pass\":\""+editarpass.Text+"\"}";



                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                try
                {
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                    StringBuilder output = new StringBuilder();
                    output.Append(reader.ReadToEnd());
                    httpResponse.Close();
                    string jsonResultado = output.ToString();
                    string message = "Su contraseña se ha cambiado con exito";
                    string caption = "Contraseña cambiada";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    
                    result = MessageBox.Show(message, caption, buttons);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                ver_perfil("http://localhost:8081/leerdatos");
                this.Refresh();
            }else
            {
                string message = "Debe introducir una contraseña antes de pulsar el botón";
                string caption = "Error Detectado al cambiar contraseña";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                result = MessageBox.Show(message, caption, buttons);

                if(result == System.Windows.Forms.DialogResult.OK)
                {
                    ver_perfil("http://localhost:8081/leerdatos");
                    this.Refresh();
                }
            }
            
        }
        private void gestionCuenta_Load(object sender, EventArgs e)
        {
            ver_perfil("http://localhost:8081/leerdatos");
            ver_direcciones("http://localhost:8081/leerdirecciones");
            ver_metodospago("http://localhost:8081/leermetodospago");
            titulo.Text += " " + username;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (metodospago.SelectedIndex >= 0)
            {

                metodoPago auxloc = (metodoPago)listaMetodosPago[metodospago.SelectedIndex];
                editarMetodo = new editarMetodoPago();
                editarMetodo.padre = this;
                editarMetodo.username = username;
                editarMetodo.objetivo = auxloc;
                editarMetodo.Show();
            }
        }

        private void editarButton_Click(object sender, EventArgs e)
        {
            editar_perfil("http://localhost:8081/editardatos");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            editar_contraseña("http://localhost:8081/editarpass");
        }

        private void direcciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (direcciones.SelectedIndex >= 0)
            {
                address auxloc = (address)listaDirecciones[direcciones.SelectedIndex];
                editarDireccion = new editar_direccion();
                editarDireccion.padre = this;
                editarDireccion.username = username;
                editarDireccion.objetivo = auxloc;
                editarDireccion.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            nuevaDireccion = new añadirDireccion();
            nuevaDireccion.padre = this;
            //nuevaDireccion.MdiParent = this;
            nuevaDireccion.username = username;
            nuevaDireccion.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            nuevoMetodo = new añadirMetodoPago();
            nuevoMetodo.padre = this;
            nuevoMetodo.username = username;
            nuevoMetodo.Show();
        }
    }
}
