﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clienteMTIS2017
{
    class perfilUsuario
    {
        public string usuario { get; set; }
        public string nombre { get; set; }
        public string cp { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }

        public perfilUsuario ()
        {
            nombre = "";
            cp = "";
            usuario = "";
            email = "";
            telefono = "";

        }
    }
}
