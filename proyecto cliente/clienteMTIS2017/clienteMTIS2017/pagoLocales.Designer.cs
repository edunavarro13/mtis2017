﻿namespace clienteMTIS2017
{
    partial class pagoLocales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pagoLocales));
            this.locales = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // locales
            // 
            this.locales.FormattingEnabled = true;
            this.locales.Location = new System.Drawing.Point(43, 40);
            this.locales.Name = "locales";
            this.locales.Size = new System.Drawing.Size(260, 329);
            this.locales.TabIndex = 0;
            this.locales.SelectedIndexChanged += new System.EventHandler(this.locales_SelectedIndexChanged);
            // 
            // pagoLocales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 406);
            this.Controls.Add(this.locales);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "pagoLocales";
            this.Text = "pagoLocales";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox locales;
    }
}