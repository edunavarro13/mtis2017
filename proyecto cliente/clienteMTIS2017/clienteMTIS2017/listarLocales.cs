﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clienteMTIS2017
{
    public partial class listarLocales : Form
    {
        List<localPago> listaLocales = new List<localPago>();
        public Form1 padre;
        private ArrayList locales;

        public listarLocales()
        {
            InitializeComponent();
            locales = new ArrayList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 5) {
                listarLocales_Load(sender, e);
                label3.Visible = false;
            }

            if (textBox1.Text.Length == 5) {
                label3.Visible = false;
                button1_Click(sender, e);
            }

            if (textBox1.Text.Length > 5) {
                label3.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            locales = new ArrayList();
            listBox1.Items.Clear();
            var url = "http://localhost:8081/codigoPostal?codp=" + textBox1.Text;
            ver_codigos_postales(url);
        }

        public void ver_codigos_postales(string url)
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                //resultado.Text = json_data;
                Char delimiter = '}';
                String[] substrings = json_data.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";
                    agregarLocales(substrings[i]);

                }
                foreach (Locales local in locales)
                {
                    listBox1.Items.Add(local.nombre);
                }

            }
        }

        public void agregarLocales(string json)
        {
            Locales l = JsonConvert.DeserializeObject<Locales>(json);
            locales.Add(l);
        }


        private void seleccionarLocal_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && !padre.listarPlatos)
            {
                int id = 0;
                if (locales.Count > 0)
                {
                    Locales auxloc = (Locales)locales[listBox1.SelectedIndex];
                    id = auxloc.id;
                }
                else
                    id = listaLocales[listBox1.SelectedIndex].id;
                string bd = "local" + id + "mtis2017";
                padre.localConsultar = bd;
                padre.bdLocalConsultar = "{ \"bd\":\"jdbc:mysql://localhost:3306/" + bd + "\"}";
                padre.abrirListarPlatos();
                padre.listarLocales = false;
                this.Close();
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            seleccionarLocal_Click(sender, e);
        }

        private void listarLocales_FormClosed(object sender, FormClosedEventArgs e)
        {
            padre.listarLocales = false;
        }

        private void listarLocales_Load(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listaLocales.Clear();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:8081/listarlocales");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                StringBuilder output = new StringBuilder();
                output.Append(reader.ReadToEnd());
                httpResponse.Close();
                string jsonResultado = output.ToString();


                Char delimiter = '}';
                String[] substrings = jsonResultado.Split(delimiter);
                for (int i = 0; i < (substrings.Length - 1); i++) // El tamaño es 1 mas, que tiene solo "]"
                {
                    String aux = substrings[i].Substring(1);
                    substrings[i] = aux + "}";

                    localPago sl = JsonConvert.DeserializeObject<localPago>(substrings[i]);
                    listaLocales.Add(sl);
                }
                foreach (localPago local in listaLocales)
                {
                    listBox1.Items.Add(local.nombre);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción producida", "excepción", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
