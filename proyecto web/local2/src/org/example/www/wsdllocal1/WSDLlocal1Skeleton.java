
/**
 * WSDLlocal1Skeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
    package org.example.www.wsdllocal1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import javax.swing.JOptionPane;

/**
     *  WSDLlocal1Skeleton java skeleton for the axisService
     */
    public class WSDLlocal1Skeleton{
        
         
    	public static String convert( ResultSet rs )throws Exception
    	{
    		if(rs.first() == false) {return "[]";} else {rs.beforeFirst();} // empty rs
    		  StringBuilder sb=new StringBuilder();
    		  Object item; String value;
    		  java.sql.ResultSetMetaData rsmd = rs.getMetaData();
    		  int numColumns = rsmd.getColumnCount();

    		  sb.append("[{");
    		  while (rs.next()) {

    		    for (int i = 1; i < numColumns + 1; i++) {
    		        String column_name = rsmd.getColumnName(i);
    		        item=rs.getObject(i);
    		        if (item !=null )
    		           {value = item.toString(); value=value.replace('"', '\'');}
    		        else 
    		           {value = "null";}
    		        sb.append("\"" + column_name+ "\":\"" + value +"\",");

    		    }                                   //end For = end record

    		    sb.setCharAt(sb.length()-1, '}');   //replace last comma with curly bracket
    		    sb.append(",{");
    		 }                                      // end While = end resultset

    		 sb.delete(sb.length()-3, sb.length()); //delete last two chars
    		 sb.append("}]");

    		 return sb.toString();
    	}
    	
    	
    	
    	
    	
        /**
         * Auto generated method signature
         * 
                                     * @param listarServicios 
             * @return listarServiciosResponse 
         */  	
    	
         public org.example.www.wsdllocal1.ListarServiciosResponse listarServicios(org.example.www.wsdllocal1.ListarServicios listarServicios)
         {
        	 org.example.www.wsdllocal1.ListarServiciosResponse resultado= new org.example.www.wsdllocal1.ListarServiciosResponse();
        	 resultado.setListaServicios("");
        	 
        	 try{	
         		 Class.forName("org.gjt.mm.mysql.Driver");
         	 }catch(Exception e)
         	 {
         		 JOptionPane.showMessageDialog(null,e);
         	 }
         	 
         		 String url = "jdbc:mysql://localhost:3306/local1mtis2017";
         		 String user = "root";
         		 String pass = "";
         		 System.out.println("Conectando...");
         		 
         		try{
        			 Connection cn = DriverManager.getConnection(url,user,pass);
        			 //Connection connection = DriverManager.getConnection(url, user,pass);
	        		 System.out.println("Conectado!!");
        			 java.sql.Connection conect=cn;
	        		 //Statement s = cn.createStatement();
  
                			 if(conect!=null)
                			 {
                				 PreparedStatement consulta= conect.prepareStatement("select * from servicios");
        				 java.sql.ResultSet r = consulta.executeQuery();
        				 resultado.setListaServicios(convert(r));
        				 cn.close();
        			 } 
        		 }catch(Exception e){
        			 System.out.println(e.getMessage());
        		 }
         		System.out.println(resultado.getListaServicios());
  			return resultado;
         }
    }
    