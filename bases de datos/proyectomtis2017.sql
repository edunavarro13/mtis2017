-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2017 a las 12:35:50
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectomtis2017`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `poblacion` varchar(70) NOT NULL,
  `ciudad` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `direccion`, `idUsuario`, `cp`, `poblacion`, `ciudad`) VALUES
(4, 'cale gelatto di cardinale nÂº 1 4ÂºF', 1, '03690', 'San Vicent del Raspeig', 'Alicante'),
(5, 'calle I don\'t see you moving nÂº 8 1ÂºB', 1, '03660', 'Novelda', 'Alicante'),
(6, 'calle le hemos echado muchos cojones nÂº 4 5ÂºC', 1, '48765', 'Madrid', 'Madrid'),
(8, 'calle primera nÂº1 1ÂºA', 2, '03690', 'Sant Vicent del Raspeig', 'Alicante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locales`
--

CREATE TABLE `locales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(800) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `direccion` varchar(800) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `poblacion` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `horario` varchar(100) NOT NULL,
  `especialidad` varchar(100) NOT NULL,
  `CIF` varchar(15) NOT NULL,
  `bd` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `locales`
--

INSERT INTO `locales` (`id`, `nombre`, `cp`, `direccion`, `telefono`, `poblacion`, `ciudad`, `horario`, `especialidad`, `CIF`, `bd`) VALUES
(1, 'Kebab De La Esquina', '03690', 'C/ de las Piscinas 34', '654321987', 'San Vicente del Raspeig', 'Alicante', '10:30 - 16:30 / 19:45 - 00:30', 'Hamburguesas', '14578264 - J', 'jdbc:mysql://localhost:3306/local1mtis2017'),
(5, 'Amareto', '03002', 'Julian Besteiro n6', '433256565', 'Alicante', 'Alicante', '00-20Ç:00', 'anchoas', '34343', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodopago`
--

CREATE TABLE `metodopago` (
  `id` int(11) NOT NULL,
  `usuario` int(11) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `valor` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `metodopago`
--

INSERT INTO `metodopago` (`id`, `usuario`, `tipo`, `valor`) VALUES
(3, 1, 'tarjeta', '4166545968211247'),
(5, 2, 'paypal', 'correoeditadogmail.com'),
(6, 2, 'paypal', 'edunavarro@gmail.com'),
(7, 1, 'Tarjeta', '4166545421121495'),
(8, 1, 'Tarjeta', '4166545898745214'),
(9, 1, 'Tarjeta', '4455789885214521'),
(10, 1, 'Tarjeta', '478951424514247'),
(12, 1, 'Paypal', 'Creoqueyavabien@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `descripción` varchar(1200) NOT NULL,
  `total` double NOT NULL,
  `idLocal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `descripción`, `total`, `idLocal`) VALUES
(1, '', 4, 1),
(2, '', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicidad`
--

CREATE TABLE `publicidad` (
  `oferta` varchar(255) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `idLocal` int(11) NOT NULL,
  `xml` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicidad`
--

INSERT INTO `publicidad` (`oferta`, `fecha`, `codigo`, `idLocal`, `xml`) VALUES
('Oferta de kebab', '12/3/2012', '3', 2, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de chino', '12/3/2012', '3', 2, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de chino', '12/3/2012', '3', 1, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de ruso', '12/3/2012', '2', 1, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de ruso', '12/3/2012', '2', 5, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de pate', '12/3/2012', '2', 5, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de kebab', '12/3/2012', '2', 5, '<h1>Gran oferta de kebab</h1><h6>Kebab a 2X1</h6>'),
('Oferta de fideos chinos', '12/5/2017', '3', 1, '<h1>Disfrute durante un tiempo limitado de nuestros fideos chinos estilo sichuan a mitad de precio</h1>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `username` varchar(300) DEFAULT NULL,
  `password` varchar(300) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fechaPubli` date NOT NULL,
  `cp` varchar(11) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `username`, `password`, `nombre`, `email`, `fechaPubli`, `cp`, `telefono`, `admin`) VALUES
(1, 'Fran', 'fran', 'Francisco Javier Algobia', 'franesp91@gmail.com', '0000-00-00', '03660', '699879879', 0),
(2, 'Santo', 'usuario', 'Vicentico', 'santo@gmail.com', '0000-00-00', '3660', '654789321', 0),
(3, 'Edu', 'edu', 'Eduardo', 'edunavarro13@gmail.com', '0000-00-00', '3002', '652547589', 0),
(4, 'marcos', 'marcos', 'marcos', 'marcos@gmail.com', '0000-00-00', '03690', '635874123', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_old`
--

CREATE TABLE `usuarios_old` (
  `id` int(11) NOT NULL,
  `username` varchar(300) DEFAULT NULL,
  `password` varchar(300) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fechaPubli` date NOT NULL,
  `cp` varchar(11) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_old`
--

INSERT INTO `usuarios_old` (`id`, `username`, `password`, `nombre`, `email`, `fechaPubli`, `cp`, `telefono`) VALUES
(0, 'Fran', 'fran', 'Francisco Javier Algobia', 'franesp91@gmail.com', '0000-00-00', '03660', '699879879'),
(1, 'Santo', 'usuario', 'Vicentico', 'santo@gmail.com', '0000-00-00', '3660', '654789321'),
(2, 'Edu', 'edu', 'Eduardo', 'edunavarro13@gmail.com', '0000-00-00', '3002', '652547589');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `locales`
--
ALTER TABLE `locales`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `metodopago`
--
ALTER TABLE `metodopago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `idLocal` (`idLocal`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `id` (`Id`) USING BTREE;

--
-- Indices de la tabla `usuarios_old`
--
ALTER TABLE `usuarios_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `metodopago`
--
ALTER TABLE `metodopago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD CONSTRAINT `direcciones_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `metodopago`
--
ALTER TABLE `metodopago`
  ADD CONSTRAINT `metodopago_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`Id`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_2` FOREIGN KEY (`idLocal`) REFERENCES `locales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
