-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2017 a las 12:35:40
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `local1mtis2017`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linped`
--

CREATE TABLE `linped` (
  `id` int(11) NOT NULL,
  `idServicio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `idPedido` int(11) NOT NULL,
  `precioLinea` double NOT NULL,
  `observaciones` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linped`
--

INSERT INTO `linped` (`id`, `idServicio`, `cantidad`, `idPedido`, `precioLinea`, `observaciones`) VALUES
(1, 1, 6, 16, 15, 'Ninguna'),
(2, 3, 4, 17, 14, 'Ninguna'),
(3, 4, 2, 18, 5, 'Ninguna'),
(4, 3, 4, 18, 14, 'Ninguna'),
(5, 2, 1, 18, 1.5, 'Ninguna'),
(6, 1, 7, 18, 17.5, 'Ninguna'),
(7, 4, 1, 19, 2.5, 'Ninguna'),
(8, 4, 5, 20, 12.5, 'Ninguna'),
(9, 4, 4, 21, 10, 'Ninguna'),
(10, 2, 1, 22, 1.5, 'Ninguna'),
(11, 4, 1, 22, 2.5, 'Ninguna'),
(12, 2, 1, 23, 1.5, 'Ninguna'),
(13, 4, 1, 23, 2.5, 'Ninguna'),
(14, 3, 1, 23, 3.5, 'Ninguna'),
(15, 1, 1, 23, 2.5, 'sodfuweribfywefiwenhfwegfvwersvgf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pago` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `fecha`, `pago`) VALUES
(1, '2016-03-09 08:29:24', 158),
(2, '2017-05-30 19:53:18', 53.6),
(3, '2017-05-30 19:53:25', 53.6),
(4, '2017-05-30 19:55:08', 53.6),
(5, '2017-05-30 19:58:05', 53.6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `precioTotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `fecha`, `precioTotal`) VALUES
(1, '2013-07-23', 20),
(2, '2013-07-23', 20),
(3, '2013-07-23', 20),
(4, '2013-07-23', 20),
(5, '2013-07-23', 20),
(6, '2013-07-23', 20),
(7, '2013-07-23', 20),
(8, '2013-07-23', 20),
(9, '2013-07-23', 20),
(10, '2013-07-23', 20),
(11, '2013-07-23', 20),
(12, '2013-07-23', 20),
(13, '2013-07-23', 20),
(14, '2013-07-23', 20),
(15, '2013-07-23', 20),
(16, '2017-05-30', 15),
(17, '2017-05-30', 14),
(18, '2017-05-30', 38),
(19, '2017-05-30', 2.5),
(20, '2017-05-31', 12.5),
(21, '2017-05-31', 10),
(22, '2017-05-31', 4),
(23, '2017-05-31', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `precio` double NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `nombre`, `descripcion`, `precio`, `tipo`) VALUES
(1, 'Hamburguesa con queso', 'Pan de hamburguesa, hamburguesa y queso', 2.5, 'Plato'),
(2, 'Coca-Cola normal', 'Bebida azucarada sabor cola', 1.5, 'Bebida'),
(3, 'Patatas Bacon&Cheese', 'Patatas fritas estilo \"palo\" con bacon y queso', 3.5, 'Entrante'),
(4, 'Tarta de queso con arándanos', 'Tarta de queso con base de galleta y confitura de arándanos por encima', 2.5, 'Postre');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `linped`
--
ALTER TABLE `linped`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idServicio` (`idServicio`),
  ADD KEY `idPedido` (`idPedido`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `linped`
--
ALTER TABLE `linped`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `linped`
--
ALTER TABLE `linped`
  ADD CONSTRAINT `linped_ibfk_1` FOREIGN KEY (`idServicio`) REFERENCES `servicios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `linped_ibfk_2` FOREIGN KEY (`idPedido`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
